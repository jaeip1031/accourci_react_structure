// src/components/Login.js
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import '../Login/Login.scss';

function Login() {
    const navigate = useNavigate();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await axios.post('', { email, password }, {
                withCredentials: true
            });
            if (response.data.result === 't') {
                navigate('/'); // 로그인 성공 시 메인 페이지로 리디렉트
            } else {
                alert('로그인에 실패했습니다. 다시 시도해주세요.');
            }
        } catch (error) {
            console.error('Login failed:', error);
            alert('로그인에 실패했습니다. 다시 시도해주세요.');
        }
    };

    return (
        <div className='login_wrap flex_center'>
            <div className="login_box flex_center flex_wrap">
                <div className="lg_input_wrap">
                    <form onSubmit={handleSubmit}>
                        <div className="lg_email_input_box lg_input_box">
                            <input
                                type="text"
                                autoFocus
                                placeholder='Email'
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                            />
                        </div>
                        <div className="lg_pw_input_box lg_input_box">
                            <input
                                type="password"
                                placeholder='Password'
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                            />
                        </div>
                        <div className="lg_btn_box">
                            <button type="submit"><span className='f_bigger f_400 f_white'>로그인</span></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default Login;
