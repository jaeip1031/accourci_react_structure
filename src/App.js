// src/App.js
import React from 'react';
import { BrowserRouter as Router, Routes, Route, Navigate } from 'react-router-dom';
import { Login, Main } from "./pages";
import Header from './Header';
import RequireAuth from './RequireAuth';

function AppContent() {
    return (
        <div className="App">
            <Routes>
                <Route path="/login" element={<Login />} />
                <Route path="*" element={
                    <RequireAuth>
                        <Routes>
                            <Route path="/" element={<><Header /><Main /></>} />
                            <Route path="*" element={<div>없는 페이지야</div>} />
                        </Routes>
                    </RequireAuth>
                } />
            </Routes>
        </div>
    );
}

function App() {
    return (
        <Router>
            <AppContent />
        </Router>
    );
}

export default App;
