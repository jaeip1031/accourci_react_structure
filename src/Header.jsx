// src/components/Header.js
import React, { useState, useEffect } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import axios from 'axios';

function Header() {
    const navigate = useNavigate();
    const location = useLocation();
    const [headerClass, setHeaderClass] = useState('');

    useEffect(() => {
        // 경로에 따라 headerClass 상태를 업데이트
        switch (location.pathname) {
            case '/':
                setHeaderClass('main-header'); // 메인 페이지에 적용할 클래스
                break;
            case '/user':
                setHeaderClass('a-header'); // A 컴포넌트 페이지에 적용할 클래스
                break;
            default:
                setHeaderClass('');
        }
    }, [location.pathname]); // 위치가 변경될 때마다 실행

    const handleLogout = async (e) => {
        e.preventDefault();
        try {
            const response = await axios.post('', {}, {
                withCredentials: true
            });
            if (response.data.result === 't') {
                navigate('/login'); // 로그아웃 성공 시 로그인 페이지로 리디렉트
            } else {
                alert('로그아웃에 실패했습니다. 다시 시도해주세요.');
            }
        } catch (error) {
            console.error('Logout failed:', error);
            alert('로그아웃에 실패했습니다. 다시 시도해주세요.');
        }
    };

    return (
        <div className={`header_wrap ${headerClass}`}>
            <div className="flex_between header">
                <div className="hd_right_box display_flex">
                    <a href="/" onClick={(e) => e.preventDefault()} className="go_web_btn hd_btn">
                        <span className="f_small f_500">웹페이지</span>
                    </a>
                    <a href="/" onClick={handleLogout} className="logout_btn hd_btn">
                        <span className="f_small">로그아웃</span>
                    </a>
                </div>
            </div>
            <nav>
            </nav>
        </div>
    );
}

export default Header;
